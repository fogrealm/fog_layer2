FROM ruby:2.4.1
RUN apt-get update && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /fog_platform
WORKDIR /fog_platform
COPY Gemfile /fog_platform/Gemfile
COPY Gemfile.lock /fog_platform/Gemfile.lock
RUN gem install bundler
RUN bundle install
COPY . /fog_platform
