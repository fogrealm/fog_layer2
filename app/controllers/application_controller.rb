class ApplicationController < ActionController::Base
  
  respond_to :html, :json

  # this enables DSL supported by inherited resource.
  # use it to override actions after actions
  include InheritedResources::DSL
  include ActionControllerBaseCommon::Exceptions
  include ActionControllerBaseCommon::DeviseAuthentications

  # collection override for inherited resource
  # this adds pagination automatically for all index actions
  # def collection
  #   get_collection_ivar || set_collection_ivar(end_of_association_chain.page(params[:page]).per(25))
  # end

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  
  after_filter do
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end
  

  def verified_request?
    # super || form_authenticity_token == request.headers['X-XSRF-TOKEN'] Rails 4.1 & below
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end


  def apply_has_scopes
    model_name = controller_name.classify.constantize
    resource_collection_variable_string = model_name.to_s.downcase.pluralize

    eval("
      @#{resource_collection_variable_string} ||= #{model_name}.all
      @#{resource_collection_variable_string} = apply_scopes(@#{resource_collection_variable_string}).all
    ")
  end


  def set_member_resource_variable
    model_name = controller_name.classify.constantize
    eval("@#{model_name.to_s.downcase} = #{model_name}.find(params[:id])")
  end

end
