class EndDeviceInfoInfraParametersController < InheritedResources::Base

  respond_to :json

  def synchronize_data
    # => Synchronize the fog layer 2 data with information send from fog layer 1 ...
    
    puts "\n\n end device info infra parameters json - #{params[:end_device_info_infra_parameters_json]}"
    end_device_info_infra_parameters_json = params[:end_device_info_infra_parameters_json]

    if end_device_info_infra_parameters_json.present?

      current_existing_ids = end_device_info_infra_parameters_json.map{|end_device_info_infra_parameter_json| end_device_info_infra_parameter_json["id"]}
      end_device_info_infra_parameters_to_be_removed = EndDeviceInfoInfraParameter.where.not(id: current_existing_ids)
      end_device_info_infra_parameters_to_be_removed.destroy_all

      end_device_info_infra_parameters_json.each do |end_device_info_infra_parameter_json|
        id = end_device_info_infra_parameter_json["id"]
        end_device_info_id = end_device_info_infra_parameter_json["end_device_info_id"]
        infra_parameter_id = end_device_info_infra_parameter_json["infra_parameter_id"]
        
        end_device_info_infra_parameter = EndDeviceInfoInfraParameter.where(id: id).first

        if end_device_info_infra_parameter.nil?
          end_device_info_infra_parameter = EndDeviceInfoInfraParameter.new
        end

        end_device_info_infra_parameter.id = id
        end_device_info_infra_parameter.end_device_info_id = end_device_info_id
        end_device_info_infra_parameter.infra_parameter_id = infra_parameter_id
        
        end_device_info_infra_parameter.save!
      end
    end
  
  end

end