class EndDeviceInfosController < InheritedResources::Base
  respond_to :json

  def synchronize_data
    # => Synchronize the fog layer 2 data with information send from fog layer 1 ...
    
    puts "\n\n end device infos - #{params[:end_device_infos_json]}"
    end_device_infos_json = params[:end_device_infos_json]

    if end_device_infos_json.present?

      current_existing_ids = end_device_infos_json.map{|end_device_info_json| end_device_info_json["id"]}
      end_device_infos_to_be_removed = EndDeviceInfo.where.not(id: current_existing_ids)
      end_device_infos_to_be_removed.destroy_all

      end_device_infos_json.each do |end_device_info_json|
        id = end_device_info_json["id"]
        name = end_device_info_json["name"]
        description = end_device_info_json["description"]
        model = end_device_info_json["model"]
        manufacturer = end_device_info_json["manufacturer"]

        end_device_info = EndDeviceInfo.where(id: id).first

        if end_device_info.nil?
          end_device_info = EndDeviceInfo.new
        end

        end_device_info.id = id
        end_device_info.name = name
        end_device_info.description = description
        end_device_info.model = model
        end_device_info.manufacturer = manufacturer

        end_device_info.save!
      end
    end
  end  
end