class EndDevicesController < InheritedResources::Base
  respond_to :json
  belongs_to :house

  def synchronize_data
    # => Synchronize the fog layer 2 data with information send from fog layer 1 ...
    
    puts "\n\n end devices json - #{params[:end_devices_json]}"
    end_devices_json = params[:end_devices_json]

    if end_devices_json.present?

      current_existing_ids = end_devices_json.map{|end_device_json| end_device_json["id"]}
      end_devices_to_be_removed = EndDevice.where.not(id: current_existing_ids)
      end_devices_to_be_removed.destroy_all

      end_devices_json.each do |end_device_json|
        id = end_device_json["id"]
        name = end_device_json["name"]
        house_section_id = end_device_json["house_section_id"]
        end_device_info_id = end_device_json["end_device_info_id"]
        service_infra_id = end_device_json["service_infra_id"]
        should_alert_status = end_device_json["should_alert_status"]
        serial_number = end_device_json["serial_number"]

        end_device = EndDevice.where(id: id).first

        if end_device.nil?
          end_device = EndDevice.new
        end

        end_device.id = id
        end_device.name = name
        end_device.house_section_id = house_section_id
        end_device.end_device_info_id = end_device_info_id
        end_device.service_infra_id = service_infra_id
        end_device.should_alert_status = should_alert_status
        end_device.serial_number = serial_number

        end_device.save!
      end
    end
    
  end

end