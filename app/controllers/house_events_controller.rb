class HouseEventsController < InheritedResources::Base
  respond_to :json


  def fire_alarm_controller
    
    service_task_entity_map = params[:service_task_entity_map]

    Rails.logger.info "\n Reaching here - fire alarm controller"
    
    valid_request = true

    valid_request = false if service_task_entity_map.nil? || service_task_entity_map[:infra_reading].nil? || service_task_entity_map[:infra_reading][:infra_parameter].nil? || service_task_entity_map[:infra_reading][:infra_parameter][:id].nil?

    Rails.logger.info "\n valid request -> #{valid_request} -> #{service_task_entity_map}"

    valid_request = false if service_task_entity_map.nil? || service_task_entity_map[:end_device_id].nil?


    if valid_request == true
      infra_parameter_id = service_task_entity_map[:infra_reading][:infra_parameter][:id]
      end_device_id = service_task_entity_map[:end_device_id]
      measurement = service_task_entity_map[:infra_reading][:value]
     
      $redis.set("controller_cached_end_device_id_#{end_device_id}_infra_parameter_id_#{infra_parameter_id}_reading", measurement)
      #result = $redis.get("controller_end_device_id_#{end_device_id}_infra_parameter_id_#{infra_parameter_id}_reading", measurement)
     
      Rails.logger.info "\n\n map received -> #{service_task_entity_map} - {result}"
      
      # => Trying scheduling them in initializers instead of here ...
      # => Do not remove it now ...
      HouseEvent.monitor_fire_occurence if HouseEvent.fire_detection_scheduler.nil? 
      HouseEvent.monitor_environment if HouseEvent.monitor_environment_scheduler.nil?

      render json: {
        status: 200,
        message: "Information is cached in controller"
      } and return
    else 
      
      render json: {
        status: 422,
        error: "Message from Rails Controller Instance Invalid Request from "
      } and return
    end
  end


  def smart_meter_controller
    service_task_entity_map = params[:service_task_entity_map]

    Rails.logger.info "\n Reaching here - smart meter controller" 
    
    valid_request = true

    valid_request = false if service_task_entity_map.nil? || service_task_entity_map[:infra_reading].nil? || service_task_entity_map[:infra_reading][:infra_parameter].nil? || service_task_entity_map[:infra_reading][:infra_parameter][:id].nil?

    Rails.logger.info "\n valid request -> #{valid_request} -> #{service_task_entity_map}"

    valid_request = false if service_task_entity_map.nil? || service_task_entity_map[:end_device_id].nil?


    if valid_request == true
      infra_parameter_id = service_task_entity_map[:infra_reading][:infra_parameter][:id]
      end_device_id = service_task_entity_map[:end_device_id]
      measurement = service_task_entity_map[:infra_reading][:value]

      $redis.set("controller_cached_end_device_id_#{end_device_id}_infra_parameter_id_#{infra_parameter_id}_reading", measurement)
      #result = $redis.get("controller_end_device_id_#{end_device_id}_infra_parameter_id_#{infra_parameter_id}_reading", measurement)
     
      Rails.logger.info "\n\n map received -> #{service_task_entity_map} - {result}"
      
      # => Trying scheduling them in initializers instead of here ...
      # => Do not remove it now ...
      HouseEvent.monitor_smart_meter if HouseEvent.smart_meter_scheduler.nil?

      render json: {
        status: 200,
        message: "Information is cached in controller"
      } and return
    else 
      
      render json: {
        status: 422,
        error: "Message from Rails Controller Instance Invalid Request from "
      } and return
    end

  end


  def test_events_controller
    render json: {
      status: 200,
      message: "Success"
    } and return
  end


  private

  def permitted_params
    params.permit(house_event: [:id, :name, :event_level, :house_event_type, :end_device_id, :reported_at])
  end

end