class HouseSectionsController < InheritedResources::Base
  respond_to :json
  belongs_to :house


  def synchronize_data
    # => Synchronize the fog layer 2 data with information send from fog layer 1 ...
    
    puts "\n\n house sections json - #{params[:house_sections_json]}"
    house_sections_json = params[:house_sections_json]

    if house_sections_json.present?
      
      current_existing_ids = house_sections_json.map{|house_section_json| house_section_json["id"]}
      house_sections_to_be_removed = HouseSection.where.not(id: current_existing_ids)
      house_sections_to_be_removed.destroy_all


      house_sections_json.each do |house_section_json|
        id = house_section_json["id"]
        name = house_section_json["name"]
        house_id = house_section_json["house_id"]

        house_section = HouseSection.where(id: id).first

        if house_section.nil?
          house_section = HouseSection.new
        end
      
        house_section.id = id
        house_section.name = name
        house_section.house_id = house_id
        house_section.save!
      
      end
    end
    
  end
end