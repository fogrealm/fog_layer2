class HousesController < InheritedResources::Base
  respond_to :json

  def synchronize_data
    # => Synchronize the fog layer 2 data with information send from fog layer 1 ...
    
    puts "\n\n current house json - #{params[:current_house_json]}"
    current_house_json = params[:current_house_json]

    if current_house_json.present?
      id = current_house_json["id"]
      name = current_house_json["name"]
      address = current_house_json["address"]
      landmark = current_house_json["description"]

      location_id = current_house_json["location"]["id"]
      location_latitude = current_house_json["location"]["latitude"]
      location_longitude = current_house_json["location"]["longitude"]

      icfn_id = icfn_base_url = cloud_id = cloud_base_url = nil
      icfn_channel_key = cloud_channel_key = nil

      nearby_icfn_node_json = current_house_json["nearby_icfn_node"]
      if nearby_icfn_node_json.present?
        icfn_id = current_house_json["nearby_icfn_node"]["icfn_id"]
        icfn_base_url = current_house_json["nearby_icfn_node"]["icfn_base_url"]
        icfn_channel_key = current_house_json["nearby_icfn_node"]["icfn_channel_key"]
      end

      nearby_cloud_server_json = current_house_json["nearby_cloud_server"]
      if nearby_cloud_server_json.present?
        cloud_id = current_house_json["nearby_cloud_server"]["cloud_id"]
        cloud_base_url = current_house_json["nearby_cloud_server"]["cloud_base_url"]
        cloud_channel_key = current_house_json["nearby_cloud_server"]["cloud_channel_key"]
      end


      house = House.where(id: id).first

      if house.present?
        house.id = id
        house.name = name
        house.address = address
        house.landmark = landmark

        house.location.id = location_id
        house.location.latitude = location_latitude
        house.location.longitude = location_longitude

        house.icfn_id = icfn_id
        house.icfn_base_url = icfn_base_url
        house.icfn_channel_key = icfn_channel_key

        house.cloud_id = cloud_id
        house.cloud_base_url = cloud_base_url
        house.cloud_channel_key = cloud_channel_key

        house.save!
      else
        house = House.new
        house.id = id
        house.name = name
        house.address = address
        house.landmark = landmark

        location = house.build_location
        location.id = location_id
        location.latitude = location_latitude
        location.longitude = location_longitude

        house.icfn_id = icfn_id
        house.icfn_base_url = icfn_base_url
        house.icfn_channel_key = icfn_channel_key

        house.cloud_id = cloud_id
        house.cloud_base_url = cloud_base_url
        house.cloud_channel_key = cloud_channel_key

        house.save!
      end

    end  
  end

  def update_icfn_base_url
    if params[:icfn_base_url].present?
      @house = House.find(params[:id])

      @house.icfn_base_url = params[:icfn_base_url]
      @house.save!

      render json: {
        :message => "Successfully updated icfn base url"
      }
    else
      render json: {
        :error => "Invalid Parameters"
      }
    end
  end
end