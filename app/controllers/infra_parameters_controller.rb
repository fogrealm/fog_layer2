class InfraParametersController < InheritedResources::Base
  respond_to :json

  def synchronize_data
    # => Synchronize the fog layer 2 data with information send from fog layer 1 ...
    
    puts "\n\n infra parameters json - #{params[:infra_parameters_json]}"
    infra_parameters_json = params[:infra_parameters_json]

    if infra_parameters_json.present?

      current_existing_ids = infra_parameters_json.map{|infra_parameter_json| infra_parameter_json["id"]}
      infra_parameters_to_be_removed = InfraParameter.where.not(id: current_existing_ids)
      infra_parameters_to_be_removed.destroy_all

      infra_parameters_json.each do |infra_parameter_json|
        id = infra_parameter_json["id"]
        name = infra_parameter_json["name"]
        unit = infra_parameter_json["unit"]
        retrieve_type = infra_parameter_json["retrieve_type"]
        
        infra_parameter = InfraParameter.where(id: id).first

        if infra_parameter.nil?
          infra_parameter = InfraParameter.new
        end

        infra_parameter.id = id
        infra_parameter.name = name
        infra_parameter.unit = unit
        infra_parameter.retrieve_type = retrieve_type
        infra_parameter.save!
      end
    end

  end
  
end