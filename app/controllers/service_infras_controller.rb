class ServiceInfrasController < InheritedResources::Base
  respond_to :json
  belongs_to :house

  def synchronize_data
    # => Synchronize the fog layer 2 data with information send from fog layer 1 ...
    
    puts "\n\n service infras json - #{params[:service_infras_json]}"
    service_infras_json = params[:service_infras_json]

    current_house = House.first

    if service_infras_json.present?

      current_existing_ids = service_infras_json.map{|service_infra_json| service_infra_json["id"]}
      service_infras_to_be_removed = ServiceInfra.where.not(id: current_existing_ids)
      service_infras_to_be_removed.destroy_all

      service_infras_json.each do |service_infra_json|
        id = service_infra_json["id"]
        name = service_infra_json["name"]
        priority = service_infra_json["priority"]
        house_id = current_house.id
        
        service_infra = ServiceInfra.where(id: id).first

        if service_infra.nil?
          service_infra = ServiceInfra.new
        end

        service_infra.id = id
        service_infra.name = name
        service_infra.priority = priority
        service_infra.house_id = house_id
        
        service_infra.save!
      end
    end

  end


end