module Actions::HouseEvent

  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end


  module ClassMethods
    
    def monitor_fire_occurence

      return if HouseEvent.fire_detection_scheduler.present?

      HouseEvent.fire_detection_scheduler = Rufus::Scheduler.new

      house_id = House.first.id

      house_sections = ServiceInfra.first.house_sections
      house_section_ids = ServiceInfra.first.house_section_ids

      house_section_id_house_section_name_mapper = HouseSection.all.map{|hs| [hs.id, hs.name]}.to_h
      
      end_devices = ServiceInfra.first.end_devices
      end_device_ids = end_devices.map(&:id)

      end_device_id_house_section_id_mapper = end_devices.map{|end_device| [end_device.id, end_device.house_section_id]}.to_h
      house_section_id_end_device_ids_mapper = house_sections.map{|house_section| [house_section.id, house_section.end_device_ids]}.to_h
      
      infra_parameter_ids = ServiceInfra.first.infra_parameter_ids
      
      fire_detection_warning_count = 2
      
      fire_detection_scheduler.every '4s' do

        $house_events_logger.info "house_section_ids -> #{house_section_ids}"

        house_section_ids.each do |house_section_id|
          

        
          house_section_end_device_ids = house_section_id_end_device_ids_mapper[house_section_id]
          #house_section_id = end_device_id_house_section_id_mapper[end_device_id]

          house_section_name = house_section_id_house_section_name_mapper[house_section_id]

          measurement_map = {}
          house_section_end_device_ids.each do |house_section_end_device_id|
            infra_parameter_ids.each do |infra_parameter_id|
              # => The key is like this ...
              # => Because, In the same house section ...
              # => You can have two devices of same type which would read the same infra paramater ...
              # => For eg: Two Temperature sensor in Kitchen with sole purpose of reading Temperature ...

              measurement_map[[house_section_end_device_id, infra_parameter_id]] = $redis.get("controller_cached_end_device_id_#{house_section_end_device_id}_infra_parameter_id_#{infra_parameter_id}_reading")
              measurement_map[[house_section_end_device_id, infra_parameter_id]] = measurement_map[[house_section_end_device_id, infra_parameter_id]].to_f if measurement_map[[house_section_end_device_id, infra_parameter_id]].present?
            end
          end

          # => continue here ..

          # => We read temperature associated with all the devices in this house section ...
          temperature_measurements = measurement_map.select{|key, value| key[1] == 1 && value.present? }.values
          co2_measurements = measurement_map.select{|key, value| key[1] == 3 && value.present? }.values

          voltage_gs822_measurements = measurement_map.select{|key, value| key[1] == 4 && value.present? }.values
          voltage_tgs880_measurements = measurement_map.select{|key, value| key[1] == 5 && value.present? }.values

          co_measurements = measurement_map.select{|key, value| key[1] == 2 && value.present? }.values
          smoke_voltage_measurements = measurement_map.select{|key, value| key[1] == 6 && value.present? }.values

          ion_voltage_measurements = measurement_map.select{|key, value| key[1] == 7 && value.present? }.values

          fire_type = :nuisance


          $house_events_logger.info "\n\n house_section  - #{house_section_id} - house section end device ids#{house_section_end_device_ids}"
          $house_events_logger.info "measurement map - #{measurement_map}"
          $house_events_logger.info " temperatures(s) - #{temperature_measurements}" if temperature_measurements.any?        
          $house_events_logger.info " co(s) - #{co_measurements}" if co_measurements.any?
          $house_events_logger.info " co2(s) - #{co2_measurements}" if co2_measurements.present?
          $house_events_logger.info " voltage_gs822(s) - #{voltage_gs822_measurements}" if voltage_gs822_measurements.any?
          $house_events_logger.info " voltage_tgs880(s) - #{voltage_tgs880_measurements}" if voltage_tgs880_measurements.any?
          $house_events_logger.info " smoke_voltage(s) - #{smoke_voltage_measurements}" if smoke_voltage_measurements.any?
          $house_events_logger.info " ion_voltage(s) - #{ion_voltage_measurements}" if ion_voltage_measurements.any?


          if temperature_measurements.any? && co2_measurements.any?
            if co2_measurements.has_any_element_greater_than_number?(210.00)  && temperature_measurements.has_any_element_greater_than_number?(40.556)
              fire_type = :flaming
            end
          elsif voltage_gs822_measurements.any? && voltage_tgs880_measurements.any?
            if voltage_gs822_measurements.has_any_element_greater_than_number?(0.9) && voltage_tgs880_measurements.has_any_element_greater_than_number?(0.15)
              fire_type = :nuisance
            end
          elsif co_measurements.any? && co2_measurements.any? && voltage_gs822_measurements.any?
            if co_measurements.has_any_element_greater_than_number?(17.00) && co2_measurements.has_any_element_greater_than_number?(22.00) && voltage_gs822_measurements.has_any_element_greater_than_number(0.27)
              fire_type = :smoldering
            end
          end

          $house_events_logger.info " fire type - #{fire_type}"

          if fire_type != :nuisance
            fire_alarm_detection_count = $redis.get("fire_alarm_detection_count").to_i
            #fire_alarm_max_sms_send =  $redis.get("fire_alarm_max_sms_send").to_i

            $house_events_logger.info "Fire Warning"
            if fire_alarm_detection_count > fire_detection_warning_count# && $redis.get("fire_detection_occurence_time").to_i == nil
              
              # => Send Event ...
              house_section_name = HouseSection.find(house_section_id).name
              event_message = "Important! Fire Detected (#{fire_type}) - #{house_section_name}, time - #{Time.now.to_s}"
              $house_events_logger.info event_message
              
              # => Save House Event in DB ...
              
              current_fire_type_saved_in_db = $redis.get("current_fire_type_saved_in_db").try(:to_sym)

              if current_fire_type_saved_in_db != fire_type
                # => This type of Fire Detection is not reported ...
                # => So we are saving this in Database ...
                # => And after that, we send the sms, inform cloud and necessary things ...

                HouseEvent.create(
                  name: "fire_detection_#{fire_type}",
                  house_event_type: HouseEvent::HOUSE_EVENT_TYPE_ALERT,
                  house_section_id: house_section_id,
                  event_level: 5,
                  reported_at: Time.now,
                  event_message: event_message
                )
                #uncomment this later .. now commented for testing ..
                $redis.setex("current_fire_type_saved_in_db", 10 * 60, fire_type.to_s)
              end

              if $redis.get("fire_detection_occurence_time") == nil
                $redis.setex("fire_detection_occurence_time", 60 * 60, Time.now.to_i)
              end
            else 
              fire_alarm_detection_count = fire_alarm_detection_count + 1
              $redis.set("fire_alarm_detection_count", fire_alarm_detection_count)
            end
          else
            $house_events_logger.info "Nuisance. Don't worry"
          end
        end
      end

    end


    def monitor_smart_meter

      return if HouseEvent.smart_meter_scheduler.present?

      house_id = House.first.id
      end_device_ids = ServiceInfra.second.end_device_ids
      infra_parameter_ids = ServiceInfra.second.infra_parameter_ids

      HouseEvent.smart_meter_scheduler = Rufus::Scheduler.new


      HouseEvent.smart_meter_scheduler.every '10s' do
        # => Send to interconnecting fog node ...
        # => We actually compute the sum of all measurements of all house sections ...
        # => Then, the computed sum is send to the interconnecting fog node ...
        # => This is done as we are not showing complete information in the code or interconnecting fog node ...

        # => Even though in the current scenario we are dealing with one infra parameter i.e kwh ..
        # => We could have a scenario where there are multiple parameters which needs to be considered ...
        # => So, we are sending to interconnecting fog node in a mapping mechanism ...
        # => For eg: In Smart Meter Infrastructure, sum of the measurements of kwh parameter, sum of the measurements of some other parameter related to smart meter infrastructure ...

        measurements_mapper = {}

        $house_events_logger.info " Monitoring Smart Meter Data "

        end_device_ids.each do |end_device_id|  
          infra_parameter_ids.each do |infra_parameter_id|
            measurements_mapper[infra_parameter_id] = [] if measurements_mapper[infra_parameter_id].nil?
            cached_reading = $redis.get("controller_cached_end_device_id_#{end_device_id}_infra_parameter_id_#{infra_parameter_id}_reading")
            if cached_reading.present?
              measurements_mapper[infra_parameter_id] << cached_reading.to_f
            end
          end
        end


        computed_measurements = {}
        
        infra_parameter_ids.each do |infra_parameter_id|
          measurements = measurements_mapper[infra_parameter_id]
            
          if measurements.present? && measurements.any?
            computed_measurements[infra_parameter_id] = measurements.sum
          end
        end

        $house_events_logger.info " Monitor Smart Meter Data 1 - #{measurements_mapper}"

        computed_measurements = {}
        
        infra_parameter_ids.each do |infra_parameter_id|
          measurements = measurements_mapper[infra_parameter_id]
            
          if measurements.present? && measurements.any?
            computed_measurements[infra_parameter_id] = measurements.average
          end
        end
        
        $house_events_logger.info " Monitor Smart Meter Data 2 - #{computed_measurements}"


        computed_measurements.map{|infra_parameter_id, computed_measurement|
          FogNetworkManager.send_reading_info_to_interconnecting_fog_node(house_id, infra_parameter_id , computed_measurement)  
        }



      end
    end


    def monitor_environment
      return if HouseEvent.monitor_environment_scheduler.present?

      house_id = House.first.id
      end_device_ids = ServiceInfra.first.end_device_ids
      infra_parameter_ids = ServiceInfra.first.infra_parameter_ids

      HouseEvent.monitor_environment_scheduler = Rufus::Scheduler.new


      HouseEvent.monitor_environment_scheduler.every '10s' do
        # => Send to interconnecting fog node ...
        # => We actually compute the sum of all measurements of all house sections ...
        # => Then, the computed sum is send to the interconnecting fog node ...
        # => This is done as we are not showing complete information in the code or interconnecting fog node ...

        # => Here, we are taking into account infra parameters like Temperature, CO , CO2 level ...
        # => We could have a scenario where there are multiple parameters which needs to be considered ...
        # => So, we are sending to interconnecting fog node in a mapping mechanism ...
        # => For eg: In Fire Meter Infrastructure, average of the measurements of temperature parameter, CO / CO2 measurements etc ...
        measurements_mapper = {}

        $house_events_logger.info " Monitoring Environment Data "

        end_device_ids.each do |end_device_id|  
          infra_parameter_ids.each do |infra_parameter_id|
            measurements_mapper[infra_parameter_id] = [] if measurements_mapper[infra_parameter_id].nil?
            cached_reading = $redis.get("controller_cached_end_device_id_#{end_device_id}_infra_parameter_id_#{infra_parameter_id}_reading")
            if cached_reading.present?
              measurements_mapper[infra_parameter_id] << cached_reading.to_f
            end
          end
        end

        $house_events_logger.info " Monitor Environment 1 - #{measurements_mapper}"

        computed_measurements = {}
        
        infra_parameter_ids.each do |infra_parameter_id|
          measurements = measurements_mapper[infra_parameter_id]
            
          if measurements.present? && measurements.any?
            computed_measurements[infra_parameter_id] = measurements.average
          end
        end
        
        $house_events_logger.info " Monitor Environment 2 - #{computed_measurements}"


        computed_measurements.map{|infra_parameter_id, computed_measurement|
          FogNetworkManager.send_reading_info_to_interconnecting_fog_node(house_id, infra_parameter_id , computed_measurement)  
        } 

      end

    end
    


  end
  
  
  module InstanceMethods
    
  end

end