module Conditions::HouseEvent

  def is_house_event_type_alert?
    # => method to check whether the house event type is of alert type ...

    self.house_event_type == HouseEvent::HOUSE_EVENT_TYPE_ALERT
  end


  def is_house_event_type_inform?
    # => method to check whether the house event type is of inform type ...

    self.house_event_type == HouseEvent::HOUSE_EVENT_TYPE_INFORM
  end  

end