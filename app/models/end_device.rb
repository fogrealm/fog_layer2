class EndDevice < ApplicationRecord

  # => Relations ...
  has_many :infra_parameters, :through => :end_device_info 
  has_many :infra_readings
  
  belongs_to :end_device_info
  belongs_to :house_section
  belongs_to :service_infra
  

  # => Validations ...
  validates :name, presence: true
  validates :end_device_info, :house_section, presence: true
  validates :should_alert_status, inclusion: { :in => [ true, false ] }
  validates :serial_number, presence: true
  
  validates_uniqueness_of :serial_number

  
  # => Delegations ...
  delegate :house, to: :house_section

  # useful for fast creation of end devices in admin panel ...
  # before_validation do
  #  self.name = "#{self.end_device_info.name} Device  #{self.house_section.name}"
  # end

  
end