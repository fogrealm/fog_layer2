class FogMaster

  class << self

    # => Accessors ...
    attr_accessor :evaluate_slave_scheduler
    

    # => Methods ...
    def evaluate_slave_fog_nodes
      # => This service needs to be running in the Fog Master Node ...
      # => This following method will evaluate the status of the fog slave controller nodes ...

      controller_connections = {}
      port_available_map = {}

      FogNetworkManager::RAILS_INSTANCES_PORT_RANGE.to_a.each do |rails_instance_port_no|
        port_available_map[rails_instance_port_no] = true
      end

      FogNetworkManager::RAILS_INSTANCES_PORT_RANGE.to_a.each do |rails_instance_port_no|

        conn = Faraday.new( :url => "http://0.0.0.0:#{rails_instance_port_no}" ) do |faraday|
          faraday.request  :url_encoded             # form-encode POST params
          faraday.response :logger                  # log requests to STDOUT
          faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
          faraday.headers['Content-Type'] = 'application/json'
        end

        controller_connections[rails_instance_port_no] = conn
      end

      evaluate_slave_scheduler = Rufus::Scheduler.new

      evaluate_slave_scheduler.every '10s' do

        FogNetworkManager::RAILS_INSTANCES_PORT_RANGE.to_a.each do |rails_instance_port_no|
          test_connection = controller_connections[rails_instance_port_no]
          
          slave_node_active_status = true
          response = test_connection.get "/house_events/test_events_controller.json" rescue slave_node_active_status = false
        
          if slave_node_active_status == true
            $fog_master_logger.info "Fog Node with Port #{rails_instance_port_no} running successully (active) "
          else
            $fog_master_logger.info "Fog Node with Port #{rails_instance_port_no} failed (inactive)"
          end
        end
      
      end
    end
  end

end

