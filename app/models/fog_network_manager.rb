class FogNetworkManager

  INTERCONNECTING_FOG_NODE_BASE_URL = "http://0.0.0.0"
  #INTERCONNECTING_FOG_NODE_BASE_URL = "http://172.17.0.1"
  INTERCONNECTING_FOG_NODE_ROUTE_PATH_CONVERGE_EVENTS = "/house_events/converge_fog_nodes_events.json"
  INTERCONNECTING_FOG_NODE_ROUTE_PATH_INFORM_DETECTION = "/house_events.json"
  INTERCONNECTING_FOG_NODE_PORT = 3050

  # => Need to uncomment this later on with proper cloud base url ...
  #CLOUD_BASE_URL = "http://ec2-13-233-93-161.ap-south-1.compute.amazonaws.com:5000"
  CLOUD_BASE_URL = "http://0.0.0.0:5000"
  CLOUD_TEST_EVENT_ROUTE_PATH = "/house_events/test_event_controller.json"
  CLOUD_SEND_INFRA_READING_PATH = "/infra_readings.json"
  CLOUD_SEND_HOUSE_EVENT_PATH = "/house_events.json"
  RAILS_INSTANCES_PORT_RANGE = 4001 .. 4003


  # => ICFN Network connectivity details ...
  ICFN_AGENT_BASE_URL = "http://172.17.0.1:4050"
  ICFN_AGENT_NETWORK_COORDINATOR_ROUTE_PATH = "/network_coordinators/manage_fogl2_request.json"

  class << self


    def send_reading_info_to_interconnecting_fog_node(house_id, infra_parameter_id, measurement)
      
      current_house = House.first
      current_house_id = current_house.id
      current_house_icfn_base_url = current_house.icfn_base_url
      current_house_icfn_channel_key = current_house.icfn_channel_key

      fog_api = nil
      external_system_base_url = nil
      if current_house_icfn_base_url.present?
        #fog_api = FogApi.new("#{current_house_icfn_base_url}")
        external_system_base_url = "#{current_house_icfn_base_url}"
      else
        #fog_api = FogApi.new("#{INTERCONNECTING_FOG_NODE_BASE_URL}:#{INTERCONNECTING_FOG_NODE_PORT}")
        external_system_base_url = "#{INTERCONNECTING_FOG_NODE_BASE_URL}:#{INTERCONNECTING_FOG_NODE_PORT}"
      end

      fog_api = FogApi.new(ICFN_AGENT_BASE_URL)

      payload = {
        house_id: house_id,
        infra_parameter_id: infra_parameter_id,
        measurement: measurement
      }

      encrypted_payload = FogSecurityManager.symmetrically_encrypted_payload_data(nil, current_house_icfn_channel_key, payload)
      message_digest = OpenSSL::HMAC.hexdigest("SHA256", current_house_icfn_channel_key, payload.to_json)
      #response = fog_api.conn.post INTERCONNECTING_FOG_NODE_ROUTE_PATH_CONVERGE_EVENTS, payload.to_json
      
      final_payload = {
        house_id: current_house_id,
        external_system_base_url: external_system_base_url,
        method_type: :post,
        route_path: INTERCONNECTING_FOG_NODE_ROUTE_PATH_CONVERGE_EVENTS,
        encrypted_payload: encrypted_payload,
        message_digest: message_digest
      }

      response = fog_api.conn.post ICFN_AGENT_NETWORK_COORDINATOR_ROUTE_PATH, final_payload.to_json
    end


    def send_meter_reading_info_to_cloud(house_id, infra_parameter_id, measurement)
      
      current_house = House.first
      current_house_id = current_house.id
      current_house_cloud_base_url = current_house.cloud_base_url
      current_house_cloud_channel_key = current_house.cloud_channel_key

      fog_api = nil
      external_system_base_url = current_house_cloud_base_url.present? ? "#{current_house_cloud_base_url}" : "#{CLOUD_BASE_URL}"

      #external_system_base_url = CLOUD_BASE_URL
      #fog_api = FogApi.new(CLOUD_BASE_URL)
      fog_api = FogApi.new(ICFN_AGENT_BASE_URL)
        
      payload_data = {
        value: measurement,
        infra_parameter_id: infra_parameter_id,
        end_device_id: 1
      }

      payload = {}
      payload[:infra_reading] = payload_data

      $fog_master_logger.info " Send meter reading house_id - #{house_id}, infra_parameter_id - #{infra_parameter_id}, measurement - #{measurement} to cloud"
      #response = fog_api.conn.post CLOUD_SEND_INFRA_READING_PATH, payload.to_json

      encrypted_payload = FogSecurityManager.symmetrically_encrypted_payload_data(nil, current_house_cloud_base_url, payload)
      message_digest = OpenSSL::HMAC.hexdigest("SHA256", current_house_cloud_base_url, payload.to_json)

      final_payload = {
        house_id: current_house_id,
        external_system_base_url: external_system_base_url,
        method_type: :post,
        route_path: CLOUD_SEND_INFRA_READING_PATH,
        encrypted_payload: encrypted_payload,
        message_digest: message_digest
      }

      response = fog_api.conn.post ICFN_AGENT_NETWORK_COORDINATOR_ROUTE_PATH, final_payload.to_json
    end


    def send_fire_detection_info_to_interconnecting_fog_node(house_event)
      # => This API based method will inform the interconnecting node about the fire scenario detected in this localized node ...

      current_house = House.first
      current_house_id = current_house.id
      current_house_icfn_base_url = current_house.icfn_base_url
      current_house_icfn_channel_key = current_house.icfn_channel_key

      fog_api = nil
      if current_house_icfn_base_url.present?
        #fog_api = FogApi.new("#{current_house_icfn_base_url}")
        external_system_base_url = "#{current_house_icfn_base_url}"
      else
        #fog_api = FogApi.new("#{INTERCONNECTING_FOG_NODE_BASE_URL}:#{INTERCONNECTING_FOG_NODE_PORT}")
        external_system_base_url = "#{INTERCONNECTING_FOG_NODE_BASE_URL}:#{INTERCONNECTING_FOG_NODE_PORT}"
      end

      fog_api = FogApi.new(ICFN_AGENT_BASE_URL)


      payload_data = {
        name: house_event.name,
        house_event_type: house_event.house_event_type,
        event_level: house_event.event_level,
        house_id: house_event.house.try(:id),
        house_section_name: house_event.house_section.try(:name),
        reported_at_timestamp: house_event.reported_at.to_i,
        event_message: house_event.event_message
      }

      puts "\n sending payload_data #{payload_data} to ICFN_AGENT"


      payload = {}
      payload[:house_event] = payload_data

      $fog_master_logger.info " Send fire detection info - #{payload} to icfn"
      #response = fog_api.conn.post INTERCONNECTING_FOG_NODE_ROUTE_PATH_INFORM_DETECTION, payload.to_json
      
      encrypted_payload = FogSecurityManager.symmetrically_encrypted_payload_data(nil, current_house_icfn_channel_key, payload)
      message_digest = OpenSSL::HMAC.hexdigest("SHA256", current_house_icfn_channel_key, payload.to_json)

      final_payload = {
        house_id: current_house_id,
        external_system_base_url: external_system_base_url,
        method_type: :post,
        route_path: INTERCONNECTING_FOG_NODE_ROUTE_PATH_INFORM_DETECTION,
        encrypted_payload: encrypted_payload,
        message_digest: message_digest
      }

      response = fog_api.conn.post ICFN_AGENT_NETWORK_COORDINATOR_ROUTE_PATH, final_payload.to_json
    end


    def send_fire_detection_info_to_cloud(house_event)
      
      fog_api = nil
      
      current_house = House.first
      current_house_id = current_house.id
      current_house_cloud_base_url = current_house.cloud_base_url
      current_house_cloud_channel_key = current_house.cloud_channel_key

      external_system_base_url = nil
      if current_house_cloud_base_url.present?
        #fog_api = FogApi.new(current_cloud_base_url)
        external_system_base_url = current_house_cloud_base_url
      else
        #fog_api = FogApi.new(CLOUD_BASE_URL)
        external_system_base_url = CLOUD_BASE_URL
      end

      fog_api = FogApi.new(ICFN_AGENT_BASE_URL)

      payload_data = {
        name: house_event.name,
        house_event_type: house_event.house_event_type,
        event_level: house_event.event_level,
        house_id: house_event.house.try(:id),
        house_section_name: house_event.house_section.try(:name),
        reported_at_timestamp: house_event.reported_at.to_i,
        event_message: house_event.event_message
      }

      payload = {}
      payload[:house_event] = payload_data

      $fog_master_logger.info " Send fire detection info - #{payload} to icfn"
      #response = fog_api.conn.post CLOUD_SEND_HOUSE_EVENT_PATH, payload.to_json
      
      encrypted_payload = FogSecurityManager.symmetrically_encrypted_payload_data(nil, current_house_cloud_channel_key, payload)
      message_digest = OpenSSL::HMAC.hexdigest("SHA256", current_house_cloud_channel_key, payload.to_json)

      final_payload = {
        house_id: current_house_id,
        external_system_base_url: external_system_base_url,
        method_type: :post,
        route_path: CLOUD_SEND_HOUSE_EVENT_PATH,
        encrypted_payload: encrypted_payload,
        message_digest: message_digest
      }

      response = fog_api.conn.post ICFN_AGENT_NETWORK_COORDINATOR_ROUTE_PATH, final_payload.to_json
    end


    handle_asynchronously :send_reading_info_to_interconnecting_fog_node
    handle_asynchronously :send_meter_reading_info_to_cloud
    handle_asynchronously :send_fire_detection_info_to_interconnecting_fog_node
    handle_asynchronously :send_fire_detection_info_to_cloud
  end
end