class FogSecurityManager
  
  # => Constants ...
  DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME = "AES-192-CBC"
  KEY_SIZE_BYTES = 24

  
  class << self

    def symmetrically_encrypted_payload_data(symmetric_encryption_scheme = nil, encryption_key = nil, plain_payload_data_hash = {})
      # => This function will symmetrically encrypt the payload data in hash format ...
      # => The hash is converted to json string ...
      # => The json string is encrypted and the Base64 encoded format is returned ...

      raise "Invalid encryption key passed as argument" if encryption_key.nil?

      symmetric_encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME

      cipher = OpenSSL::Cipher.new(symmetric_encryption_scheme)
      cipher.encrypt

      encryption_key_padded_24_bytes = encryption_key.ljust(KEY_SIZE_BYTES, '0')
      cipher.key = encryption_key_padded_24_bytes

      plain_payload_data = plain_payload_data_hash.to_json

      encrypted_payload_data = cipher.update(plain_payload_data) + cipher.final
      Base64.encode64(encrypted_payload_data)    
    end

=begin
    def retrieve_plain_json_from_encrypted_json_list(encrypted_json_list = nil, current_house = nil)
      return if encrypted_json_list.nil? || current_house.nil?

      plain_json_list = []
      encrypted_json_list.each do |encrypted_json|
        plain_json_list << retrieve_plain_json_from_encrypted_json(encrypted_json, current_house)
      end

      plain_json_list
    end    

    def retrieve_plain_json_from_encrypted_json(encrypted_json, current_house, opts = {})
      # => The following function retrieves the plain hash based data from the json response ...
      # => The json response includes asymmetrically encrypted symmetric key information and symmetrically encrypted payload data  ..
      # => The symmetric key and initial vector are retrieved from the asymmetrically encrypted data ...
      # => The current house information is used to decrypt the asymmetrically encrypted content as the private key as the house information ...
      # => The symmetric key information retrieved is used to decrypt the encrypted data payload ...
      # => The net result is the payload data ...

      #encrypted_json = JSON.parse(encrypted_json_response.body)
      asymmetrically_encrypted_symmetric_encryption_info = Base64.decode64(encrypted_json["asymmetrically_encrypted_symmetric_encryption_info"])
      symmetrically_encrypted_payload_data = Base64.decode64(encrypted_json["symmetrically_encrypted_payload_data"])

      private_key_content = File.read("house_#{current_house.id}_private_key.pem")
      private_key = OpenSSL::PKey::RSA.new(private_key_content, nil)
      asymmetrically_encrypted_symmetric_encryption_info_json_string = private_key.private_decrypt(Base64.decode64(asymmetrically_encrypted_symmetric_encryption_info))

      asymmetrically_encrypted_symmetric_encryption_info_json = JSON.parse(asymmetrically_encrypted_symmetric_encryption_info_json_string)
      base64_encoded_symmetric_key = asymmetrically_encrypted_symmetric_encryption_info_json["symmetric_key"]
      base64_encoded_initial_vector = asymmetrically_encrypted_symmetric_encryption_info_json["initial_vector"]

      symmetric_encryption_scheme = asymmetrically_encrypted_symmetric_encryption_info_json["symmetric_encryption_scheme"]
      symmetric_key = Base64.decode64(base64_encoded_symmetric_key)
      initial_vector = Base64.decode64(base64_encoded_initial_vector)

      puts "\n encryption_info - #{symmetric_encryption_scheme}, #{symmetric_key}, #{initial_vector}"


      decipher = OpenSSL::Cipher.new(symmetric_encryption_scheme)
      decipher.decrypt
      decipher.key = symmetric_key
      decipher.iv = initial_vector

      payload_data_json_string = decipher.update(symmetrically_encrypted_payload_data) + decipher.final
      plain_json = JSON.parse(payload_data_json_string)
      plain_json
    end


    def fog_request_encoded_token_signature(current_house)
      # => The following method generates the base64 token signature ...
      # => The message signed contains house_id and house_authentication_token ...

      raise "Invalid Current House" unless current_house.is_a?(House)

      private_key_content = File.read("house_#{current_house.id}_private_key.pem")
      private_key = OpenSSL::PKey::RSA.new(private_key_content, nil)
      plain_message = { house_id: current_house.id, house_authentication_token: current_house.authentication_token }.to_json            
      signature = private_key.sign(OpenSSL::Digest::SHA256.new, plain_message)
      Base64.encode64(signature)
    end
=end
  end

end