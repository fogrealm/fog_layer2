module Formats::AdminUser

  def as_json(opts = {})
    {
      id: self.id,
      name: self.name,
      email: self.email,
      phone_number: self.phone_number
    }
  end


end