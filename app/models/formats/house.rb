module Formats::House

  def as_json(opts = {})
    {
      id: self.id,
      name: self.name,
      address: self.address,
      landmark: self.landmark, 
      description: self.description,
      location: self.location.as_json
    }
  end
  
end