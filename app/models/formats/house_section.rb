module Formats::HouseSection

  def as_json(opts = {})
    {
      id: self.id,
      name: self.name
    }
  end


  def seeding_json( opts = {} )
    {
      id: self.id,
      name: self.name,
      house_id: self.house_id
    }
  end

end