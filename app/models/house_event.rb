class HouseEvent < ApplicationRecord
  include Actions::HouseEvent
  include Conditions::HouseEvent

  # => Constants ...
  HOUSE_EVENT_TYPE_ALERT = "alert"
  HOUSE_EVENT_TYPE_INFORM = "inform"

  HOUSE_EVENT_TYPES = [
    HOUSE_EVENT_TYPE_ALERT,
    HOUSE_EVENT_TYPE_INFORM
  ]
  

  # => Relations ...
  belongs_to :house_section


  # => Delegations ...
  delegate :house, to: :house_section


  # => Validations ...
  validates :name, presence: true
  validates :house_event_type, inclusion: { :in => HOUSE_EVENT_TYPES }
  validates :event_level, inclusion: { :in => (1..10)}

  
  # => Callbacks ...
  before_validation on: :create do 
    # => If house event type is not mentioned, then we take type as 'inform' by default ...
    # => If event level is not mentioned, then we take default level as 1 ...

    if self.house_event_type.nil?
      self.house_event_type = HOUSE_EVENT_TYPE_INFORM
    end

    if self.event_level.nil?
      self.event_level = 1
    end

    if self.reported_at.nil?
      self.reported_at = Time.now
    end

    self.event_message = "" if self.event_message.nil?

  end
  
  after_save :inform_external_entities


  # => Scopes ...
  scope :event_type_alert, -> {
    where(
      :house_event_type => HOUSE_EVENT_TYPE_ALERT
    )
  }


  # => Private Methods ...
  private

  def inform_external_entities
    # => Inform External Entities about the House Event created by means of sms and cloud servers ...
    

    all_admin_users = AdminUser.all
    $house_events_logger.info " Informing Users - #{all_admin_users.as_json }} "

    phone_numbers = all_admin_users.map(&:phone_number)
    #SmsManager.send_sms(to: phone_numbers, message: self.event_message)

    $house_events_logger.info " Informing Interconnecting Node "
    FogNetworkManager.send_fire_detection_info_to_interconnecting_fog_node(self)  
     
    $house_events_logger.info " Informing Cloud Server "
    #FogNetworkManager.send_fire_detection_info_to_cloud(self)

  end


  class << self

    # => Accessors ...
    # => These are schedulers defined inside the Class HouseEvent ...

    attr_accessor :fire_detection_scheduler, :smart_meter_scheduler, :monitor_environment_scheduler

  end
end
