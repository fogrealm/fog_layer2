class InfraReading < ApplicationRecord
  #include Formats::InfraReading

  # => Relations ...
  belongs_to :end_device
  belongs_to :infra_parameter


  # => Validations ...
  validates :value, presence: true
  validates :end_device, presence: true
  validates :infra_parameter, presence: true

  validate :ensure_reading_is_correct_from_device

  # => Delegations ...
  delegate :min_value, to: :infra_parameter
  delegate :max_value, to: :infra_parameter
  delegate :unit, to: :infra_parameter


  private

  def ensure_reading_is_correct_from_device
    # => Code to check whether the infra parameter matches with the end device readings ...
    # => We refer end_device_info data for verification ...

    if self.end_device.infra_parameters.include?(self.infra_parameter) == false
      errors[:Infra_reading] << " is incorrect because this End Device cannot sense this infra parameter reading "
      return 
    end
  end

end
