Rails.application.routes.draw do
  
  root to: 'static_pages#home'

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html 

  resource :static_pages, only: [] do
    collection do
      get :home
    end
  end


  resources :house_events do
    collection do
      post :fire_alarm_controller
      post :smart_meter_controller

      get :test_events_controller
    end
  end


  resources :houses do
    member do
      post :synchronize_data
    end

    resources :end_devices do
      collection do
        post :synchronize_data
      end
    end

    resources :service_infras do
      collection do
        post :synchronize_data
      end
    end

    resources :house_sections do
      collection do
        post :synchronize_data
      end
    end    
  end


  resources :end_device_infos do
    collection do
      post :synchronize_data
    end
  end

  resources :infra_parameters do
    collection do
      post :synchronize_data
    end
  end

  resources :end_device_info_infra_parameters do
    collection do
      post :synchronize_data
    end
  end



end