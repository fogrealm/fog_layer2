class CreateHouseSections < ActiveRecord::Migration[5.0]
  def change
    create_table :house_sections do |t|
      t.references :house, index: true

      t.string :name

      t.timestamps
    end
  end
end
