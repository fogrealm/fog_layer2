class CreateServiceInfras < ActiveRecord::Migration[5.0]
  def change
    create_table :service_infras do |t|
      t.references :house, index: true

      t.string :name, index: true
      t.integer :priority, index: true

      t.timestamps
    end

    add_index :service_infras, [ :name, :house_id], unique: true
  end
end
