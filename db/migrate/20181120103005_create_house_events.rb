class CreateHouseEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :house_events do |t|
      t.references :house_section, index: true

      t.string :name
      t.string :house_event_type, default: HouseEvent::HOUSE_EVENT_TYPE_INFORM
      t.integer :event_level, default: 1

      t.datetime :reported_at

      t.text :event_message

      t.timestamps
    end
  end
end