class CreateAdminUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_users do |t|
      t.string :email, null: false, default: "", unique: true
      
      t.string :name, null: false, default: ""
      t.string :phone_number, null: false


      t.timestamps
    end
  end
end
