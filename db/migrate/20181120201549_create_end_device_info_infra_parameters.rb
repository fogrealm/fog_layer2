class CreateEndDeviceInfoInfraParameters < ActiveRecord::Migration[5.0]
  def change
    create_table :end_device_info_infra_parameters do |t|
      t.references :end_device_info, index: true
      t.references :infra_parameter, index: true

      t.timestamps
    end
  end
end































